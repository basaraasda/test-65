package com.axion.ec.a65apps;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ChoiceSpec extends Fragment {

    Context context;
    TextView choiceText;
    Spinner choiceSpinner;
    List<SpecialityDB> specialityDBList = new ArrayList<>();
    ArrayAdapter<String> spineradapter;
    String[] specList;
    String[] specialty;


    public ChoiceSpec() {
    }

    public static ChoiceSpec newInstance(int position) {
        ChoiceSpec fragment = new ChoiceSpec();
        Bundle args = fragment.getArguments();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        specialityDBList = SpecialityDB.findWithQuery(SpecialityDB.class,"SELECT DISTINCT speciality  FROM SPECIALITY_DB");
        specList = new String[specialityDBList.size()];
        for(int i = 0;i<specialityDBList.size();i++) {
            specList[i] = specialityDBList.get(i).getSpeciality();
        }

        String[] strEm = {" "};
        specialty = new String[specList.length+strEm.length];
        System.arraycopy(strEm, 0, specialty, 0, strEm.length );
        System.arraycopy(specList,0,specialty,strEm.length,specList.length);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        choiceText = (TextView) view.findViewById(R.id.textSpecChoice);
        choiceSpinner = (Spinner) view.findViewById(R.id.spinnerChoice);
        spineradapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, specialty);
        spineradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        choiceSpinner.setAdapter(spineradapter);
        choiceSpinner.setPrompt("Специальности");
        choiceSpinner.setOnItemSelectedListener(null);
        choiceSpinner.setSelection(0,false);
        choiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 0) {

                }
                else {
                   ((MainActivity) getActivity()).choiceSpeciality(position);
                   choiceSpinner.setSelection(position-position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                return;
            }
        });

        return view;
    }
}
