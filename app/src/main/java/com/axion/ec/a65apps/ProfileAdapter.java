package com.axion.ec.a65apps;


import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    private List<ProfileDB> profiles;
    private List<SpecialityDB> specialties;
    private OnCliclListner mOnCliclListner;


    public ProfileAdapter(List<ProfileDB> profiles, List<SpecialityDB> specialties, OnCliclListner onCliclListner) {
        if(profiles!=null){
            this.profiles = null;
            this.specialties = null;
        }
        this.profiles = profiles;
        this.specialties = specialties;
        this.mOnCliclListner = onCliclListner;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_item, parent, false);
        return new ViewHolder(v, mOnCliclListner);
    }

    public String firstUpperCase(String word){
        if(word == null || word.isEmpty()) return "";
        word.substring(1).toLowerCase();
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }

    public String birthdayDate(String date){
        String d = "-";
        SimpleDateFormat dateFormat;
        if (date == null){
            return d;
        }
        if(date.isEmpty()){
            return d;
        }
        if(date.substring(0,3).contains("-")){
            dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        }
        else {
             dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        }

        Date convertedDate;

        try{
            convertedDate=dateFormat.parse(date);
            SimpleDateFormat format2 = new SimpleDateFormat("dd.mm.yyyy");
             d = format2.format(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return d;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        ProfileDB profile = profiles.get(i);
        SpecialityDB specialty = specialties.get(i);
        viewHolder.name.setText(firstUpperCase(profile.getF_name()) + " " + firstUpperCase(profile.getL_name()));
        viewHolder.bithday.setText(birthdayDate(profile.getBirthday()));
        viewHolder.specialty.setText(specialty.getSpeciality());

    }


    @Override
    public int getItemCount() {
        if (profiles == null)
            return 0;
        return profiles.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView bithday;
        TextView specialty;
        OnCliclListner onCliclListner;

        public ViewHolder(View itemView, OnCliclListner onCliclListner) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            bithday = (TextView) itemView.findViewById(R.id.bithday);
            specialty = (TextView) itemView.findViewById(R.id.specialty);
            this.onCliclListner = onCliclListner;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onCliclListner.onItemClick(getAdapterPosition());

        }
    }
    public interface OnCliclListner{
        void onItemClick(int position);

    }

}

