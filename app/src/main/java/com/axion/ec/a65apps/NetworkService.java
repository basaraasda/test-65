package com.axion.ec.a65apps;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static NetworkService mInstance;
    private static final String URL = "https://gitlab.65apps.com/65gb/static/raw/master/";
    private Retrofit mRetrofit;


    private NetworkService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder client = new OkHttpClient.Builder()
                .addInterceptor(interceptor);
        mRetrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();
    }

    public static NetworkService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }
    public JSONPlaceHolderApi getJSONApi() {
        return mRetrofit.create(JSONPlaceHolderApi.class);
    }
}
class CountProfiles{
    @SerializedName("response")
    @Expose
    private List<Profiles> profilesList;

    public void setProfilesList(List<Profiles> profilesList) {
        this.profilesList = profilesList;
    }

    public List<Profiles> getProfilesList() {
        return profilesList;
    }


}
class Specialty{
    @SerializedName("specialty_id")
    @Expose
    private String specialty_id;
    @SerializedName("name")
    @Expose
    private String name;
    private int id;
    private String prof_id;

    public String getSpecialityId(){
        return specialty_id;
    }
    public void setSpecialtyId(String specialty_id){
        this.specialty_id = specialty_id;
    }
    public String getSpecialityName(){
        return name;
    }
    public void setSpecialtyName(String  name){
        this.name = name;
    }
    public int getID(){
        return id;
    }
    public void setID(int id){
        this.id = id;
    }
    public String getProfID(){
        return prof_id;
    }
    public void setprofID(String id){
        this.prof_id = id;
    }
}
class Profiles  {
    @SerializedName("f_name")
    @Expose
    private String f_name;
    @SerializedName("l_name")
    @Expose
    private String l_name;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("avatr_url")
    @Expose
    private String avatr_url;
    @SerializedName("specialty")
    @Expose
    private List<Specialty> specialtiy;
    private String id;

    public void setSpecialtiy(List<Specialty> specialtiy) {
        this.specialtiy = specialtiy;

    }

    public List<Specialty> getSpecialtiy() {
        return specialtiy;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAvatr_url() {
        return avatr_url;
    }

    public void setAvatr_url(String avatr_url) {
        this.avatr_url = avatr_url;
    }

    public String getID(){
        return id;
    }

    public void setID(String id){
        this.id = id;
    }
}