package com.axion.ec.a65apps;

import retrofit2.Call;
import retrofit2.http.GET;

interface JSONPlaceHolderApi {
    @GET("testTask.json")
    Call<CountProfiles> getProfiles();
}