package com.axion.ec.a65apps;

import com.orm.SugarRecord;

public class ProfileDB extends SugarRecord<ProfileDB> {

    private String f_name;

    private String l_name;

    private String birthday;

    private String avatr_url;

    private String uuid;


    public ProfileDB() {
    }

    public ProfileDB(String f_name, String l_name, String avatr_url, String birthday, String uuid) {
        this.f_name = f_name;
        this.l_name = l_name;
        this.avatr_url = avatr_url;
        this.birthday = birthday;
        this.uuid = uuid;
    }

    public String getF_name() { return f_name; }
    public String getL_name() { return l_name; }
    public String getAvatr_url() { return avatr_url; }
    public String getBirthday() { return birthday; }
    public String getUuid() { return uuid; }





    @Override
    public String toString() {
        return "ProfileDB{" +
                "f_name='" + f_name + '\'' +
                ", l_name='" + l_name + '\'' +
                ", avatr_url='" + avatr_url + '\'' +
                ", birthday='" + birthday + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
