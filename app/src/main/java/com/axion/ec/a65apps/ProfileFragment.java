package com.axion.ec.a65apps;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ProfileFragment extends Fragment {


    final static int PROFILE_CLICK = 0;
    Profiles profiles;
    List<ProfileDB> profileDB;
    List<SpecialityDB> specialityDBList;
    List<Specialty> specialty;
    Context context;
    TextView profileName;
    TextView birthday;
    TextView speciality;
    Bitmap avatar;
    static Handler handler;


    ImageView avatr;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance(int position) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = fragment.getArguments();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        handler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case PROFILE_CLICK:
                        avatr.setImageBitmap(avatar);
                        break;
                }
            }
        };
        String id = getArguments().getString("position");
        profileDB = ProfileDB.find(ProfileDB.class,"uuid = ?",id);
        specialityDBList = SpecialityDB.find(SpecialityDB.class,"profuuid = ?",id);
        profileName = (TextView) view.findViewById(R.id.profile_name);
        birthday = (TextView) view.findViewById(R.id.birthday);
        avatr = (ImageView) view.findViewById(R.id.avatr);
        speciality = (TextView) view.findViewById(R.id.prof_speciality);


        String log = " Id: " + profileDB.get(0).getUuid() + " ,Name: " + profileDB.get(0).getF_name()
                + " " + profileDB.get(0).getL_name() + " ,Birthday: " + profileDB.get(0).getBirthday();
        System.out.println("ID: " + id);
        System.out.println(log);
        reciveAvatar(profileDB.get(0).getAvatr_url());
        profileName.setText(firstUpperCase(profileDB.get(0).getF_name()) + " " + firstUpperCase(profileDB.get(0).getL_name()));
        birthday.setText(birthdayDate(profileDB.get(0).getBirthday()));
        speciality.setText(specialityDBList.get(0).getSpeciality());
        return view;
    }


    public String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return "";
        word.substring(1).toLowerCase();
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }

    public String birthdayDate(String date) {
        String d = "-";
        SimpleDateFormat dateFormat;
        if (date == null) {
            return d;
        }
        if (date.isEmpty()) {
            return d;
        }
        if (date.substring(0, 3).contains("-")) {
            dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        } else {
            dateFormat = new SimpleDateFormat("yyyy-mm-dd");
        }

        Date convertedDate;

        try {
            convertedDate = dateFormat.parse(date);
            SimpleDateFormat format2 = new SimpleDateFormat("dd.mm.yyyy");
            d = format2.format(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return d;
    }

    private void reciveAvatar(final String url) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    URL newurl = new URL(url);

                    avatar = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                    ProfileFragment.handler.sendEmptyMessage(0);

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

        };
        Thread thread = new Thread(runnable);
        thread.start();

    }
}
