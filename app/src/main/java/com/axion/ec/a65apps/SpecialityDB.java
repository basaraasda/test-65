package com.axion.ec.a65apps;

import com.orm.SugarRecord;

public class SpecialityDB extends SugarRecord<SpecialityDB> {

    private String speciality;

    private String speciality_id;

    private String prof_uuid;

    public SpecialityDB(){
    }

    public SpecialityDB(String speciality,String speciality_id,String prof_uuid){

        this.speciality = speciality;
        this.speciality_id = speciality_id;
        this.prof_uuid = prof_uuid;
    }

    public String getSpeciality() { return speciality; }
    public String getSpeciality_id() { return speciality_id; }
    public String getProf_uuid() { return prof_uuid; }


    @Override
    public String toString() {
        return "SpecialityDB{" +
                "speciality='" + speciality + '\'' +
                ", speciality_id='" + speciality_id + '\'' +
                ", prof_uuid='" + prof_uuid + '\'' +
                '}';
    }


}
