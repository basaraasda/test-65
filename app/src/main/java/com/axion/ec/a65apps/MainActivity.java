package com.axion.ec.a65apps;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends FragmentActivity implements ProfileAdapter.OnCliclListner {


    TextView textView;
    List<Profiles> profiles;
    List<Specialty> specialties;

    ItemFragment itemFragment ;
    ChoiceSpec choiceSpecFrag;
    ProfileFragment profileFragment;
    public FragmentManager fragmentManager = getSupportFragmentManager();

    List<ProfileDB> profileUuidList;
    List<SpecialityDB> profList = new ArrayList<>();

    FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        profiles = new ArrayList<>();
        specialties = new ArrayList<>();
        frameLayout = (FrameLayout) findViewById(R.id.frame);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LOCKED);

        itemFragment = new ItemFragment();
        choiceSpecFrag = new ChoiceSpec();

        ProfileDB.deleteAll(ProfileDB.class);
        SpecialityDB.deleteAll(SpecialityDB.class);


        NetworkService.getInstance()
                .getJSONApi()
                .getProfiles()
                .enqueue(new Callback<CountProfiles>() {
                    @Override
                    public void onResponse(@NonNull Call<CountProfiles> call, @NonNull Response<CountProfiles> response) {

                        CountProfiles countProfiles = response.body();
                        profiles.addAll(countProfiles.getProfilesList());
                        System.out.println("Inserting ..");
                        for (int i = 0; i < profiles.size(); i++) {
                            String uuid = UUID.randomUUID().toString();
                            for (Specialty specialty: profiles.get(i).getSpecialtiy()) {
                                specialty.setprofID(uuid);
                                SpecialityDB specialityDB = new SpecialityDB(
                                        specialty.getSpecialityName(),
                                        specialty.getSpecialityId(),
                                        specialty.getProfID());
                                specialityDB.save();

                            }
                            ProfileDB profileDB = new ProfileDB(
                                    profiles.get(i).getF_name(),
                                    profiles.get(i).getL_name(),
                                    profiles.get(i).getAvatr_url(),
                                    profiles.get(i).getBirthday(),
                                    uuid);
                            profileDB.save();
                        }

                        fragmentManager.beginTransaction().add(R.id.frame,choiceSpecFrag).commit();

                    }

                    @Override
                    public void onFailure(@NonNull Call<CountProfiles> call, @NonNull Throwable t) {
                        textView.append("Error occurred while getting request! " + t.getMessage());
                    }
                });
    }

    public void setAdapter(RecyclerView recycler){
        Log.d("1", "adapter");
        ProfileAdapter adapter = new ProfileAdapter(profileUuidList,profList,this);
        recycler.setAdapter(adapter);
        recycler.getAdapter().notifyDataSetChanged();
    }


    public void choiceSpeciality(int position){
        List<SpecialityDB> specialityDBS;
        profileUuidList = new ArrayList<>();
        specialityDBS = SpecialityDB.findWithQuery(SpecialityDB.class,"SELECT DISTINCT specialityid  FROM SPECIALITY_DB");
        String[] specID;
        specID= new String[specialityDBS.size()];
        for (int i =0; i<specialityDBS.size();i++){
            specID[i] = specialityDBS.get(i).getSpeciality_id();
        }
        profList = SpecialityDB.find(SpecialityDB.class,"specialityid = ?",specID[position-1]);
        String [] profuid = new String[profList.size()];

        for(int i = 0; i < profList.size();i++) {
            profuid[i] = profList.get(i).getProf_uuid();
        }
        for(int i = 0; i < profuid.length;i++) {
            profileUuidList.addAll(ProfileDB.find(ProfileDB.class, "uuid = ?", profuid[i]));
        }
        fragmentManager.beginTransaction().replace(R.id.frame,itemFragment).addToBackStack(null).commit();
    }

    @Override
    public void onItemClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putString("position", profileUuidList.get(position).getUuid());
        profileFragment = new ProfileFragment();
        profileFragment.setArguments(bundle);
        fragmentManager.beginTransaction().replace(R.id.frame,profileFragment).addToBackStack(null).commit();
        Log.d("CLICK", "CLICK "+position);

    }
}

